package fr.ratslab.plus5;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Created by sebastientosello on 25/04/16.
 */
public class ArticleActivity extends AppCompatActivity{
    private TextView articleContent;
    private TextView articleTitle;
    private ImageView articleImg;

    private NodeList nodelist;
    private ProgressDialog pDialog;

    private String URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_activity);

        articleContent = (TextView) findViewById(R.id.content);
        articleTitle = (TextView) findViewById(R.id.bigTitle);
        articleImg = (ImageView) findViewById(R.id.imgArticle);

        //articleImg.setImageResource(R.drawable.monalisa);
        //LoadImageFromWebOperations("https://www.android.com/static/img/logos-2x/android-wordmark-8EC047.png", articleImg);

        String typeOfTheArticle;

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                typeOfTheArticle = "art";
            } else {
                typeOfTheArticle = (String)extras.getString("type");
            }
        } else {
            typeOfTheArticle = (String) savedInstanceState.getSerializable("type");
        }

        String title = getArticle(typeOfTheArticle);
        articleTitle.setText(title);

        String title4Url = title.replace(" ", "%20");//replace spaces with %20
        //URL = "https://en.wikipedia.org/w/api.php?format=xml&action=query&prop=extracts&exintro=&explaintext=&titles=" + title4Url;
        URL = "https://en.wikipedia.org/w/api.php?format=xml&action=query&prop=extracts&explaintext=&titles=" + title4Url;

        new DownloadXML().execute(URL);

    }

    private class DownloadXML extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressbar
            pDialog = new ProgressDialog(ArticleActivity.this);
            pDialog.setTitle("Récupération de l'article");
            pDialog.setMessage("Chargement...");
            pDialog.setIndeterminate(false);
            // Show progressbar
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... Url) {

            try {
                URL url = new URL(Url[0]);
                DocumentBuilderFactory dbf = DocumentBuilderFactory
                        .newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                // Download the XML file
                Document doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();
                // Locate the Tag Name
                nodelist = doc.getElementsByTagName("pages");

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (nodelist != null) {
                    Node nNode = nodelist.item(0);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        articleContent.setText(getNode("extract", eElement));
                    }
                //progressbar
                pDialog.dismiss();
            } else {
                articleContent.setText("ProblemRetrieving article");
            }
        }
    }

    // getNode function
    private static String getNode(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
                .getChildNodes();
        Node nValue = (Node) nlList.item(0);
        return nValue.getNodeValue();
    }

    private String getArticle(String typeOfArticle){

        String picked = "";

        HashMap<Integer,HashMap<String,String>> art = new HashMap<Integer, HashMap<String, String>>();
        HashMap<Integer,HashMap<String,String>> history = new HashMap<Integer, HashMap<String, String>>();
        HashMap<Integer,HashMap<String,String>> maths = new HashMap<Integer, HashMap<String, String>>();
        HashMap<Integer,HashMap<String,String>> physics = new HashMap<Integer, HashMap<String, String>>();
        HashMap<Integer,HashMap<String,String>> coding = new HashMap<Integer, HashMap<String, String>>();

        HashMap<String, String> art1 = new HashMap<String, String>();
        HashMap<String, String> art2 = new HashMap<String, String>();
        HashMap<String, String> art3 = new HashMap<String, String>();
        HashMap<String, String> art4 = new HashMap<String, String>();
        HashMap<String, String> art5 = new HashMap<String, String>();

        art1.put("Mona Lisa", "monalisa");
        art2.put("Andy Warhol", "andywarhol");
        art3.put("Vincent Van Gogh", "vangogh");
        art4.put("Mona Lisa", "monalisa");
        art5.put("Indigenous Australian art", "indigenous");

        art.put(0, art1);
        art.put(1, art2);
        art.put(2, art3);
        art.put(3, art4);
        art.put(4, art5);

        HashMap<String, String> history1 = new HashMap<String, String>();
        HashMap<String, String> history2 = new HashMap<String, String>();
        HashMap<String, String> history3 = new HashMap<String, String>();
        HashMap<String, String> history4 = new HashMap<String, String>();
        HashMap<String, String> history5 = new HashMap<String, String>();

        history1.put("Vietnam War", "vietnamwar");
        history2.put("Colonial history of the United States", "colonialus");
        history3.put("Pompeii", "pompeii");
        history4.put("Age of Enlightenment", "enlightenment");
        history5.put("Vietnam War", "vietnamwar");

        history.put(0, history1);
        history.put(1, history2);
        history.put(2, history3);
        history.put(3, history4);
        history.put(4, history5);

        HashMap<String, String> maths1 = new HashMap<String, String>();
        HashMap<String, String> maths2 = new HashMap<String, String>();
        HashMap<String, String> maths3 = new HashMap<String, String>();
        HashMap<String, String> maths4 = new HashMap<String, String>();
        HashMap<String, String> maths5 = new HashMap<String, String>();

        maths1.put("Dichotomy", "dichotomy");
        maths2.put("Dichotomy", "dichotomy");
        maths3.put("Dichotomy", "dichotomy");
        maths4.put("Dichotomy", "dichotomy");
        maths5.put("Dichotomy", "dichotomy");

        maths.put(0, maths1);
        maths.put(1, maths2);
        maths.put(2, maths3);
        maths.put(3, maths4);
        maths.put(4, maths5);

        HashMap<String, String> physics1 = new HashMap<String, String>();
        HashMap<String, String> physics2 = new HashMap<String, String>();
        HashMap<String, String> physics3 = new HashMap<String, String>();
        HashMap<String, String> physics4 = new HashMap<String, String>();
        HashMap<String, String> physics5 = new HashMap<String, String>();

        physics1.put("Diffraction", "diffraction");
        physics2.put("Diffraction", "diffraction");
        physics3.put("Diffraction", "diffraction");
        physics4.put("Diffraction", "diffraction");
        physics5.put("Diffraction", "diffraction");

        physics.put(0, physics1);
        physics.put(1, physics2);
        physics.put(2, physics3);
        physics.put(3, physics4);
        physics.put(4, physics5);

        HashMap<String, String> coding1 = new HashMap<String, String>();
        HashMap<String, String> coding2 = new HashMap<String, String>();
        HashMap<String, String> coding3 = new HashMap<String, String>();
        HashMap<String, String> coding4 = new HashMap<String, String>();
        HashMap<String, String> coding5 = new HashMap<String, String>();

        coding1.put("Stack Overflow", "stackoverflow");
        coding2.put("Brute-force attack", "bruteforce");
        coding3.put("Stack Overflow", "stackoverflow");
        coding4.put("Stack Overflow", "stackoverflow");
        coding5.put("Stack Overflow", "stackoverflow");

        coding.put(0, coding1);
        coding.put(1, coding2);
        coding.put(2, coding3);
        coding.put(3, coding4);
        coding.put(4, coding5);

        Random rand = new Random();
        int index = rand.nextInt(4 - 0) + 0;
        String variableValue;
        List<String> list;

        switch (typeOfArticle)
        {
            case "art":
                list = new ArrayList<String>(art.get(index).keySet());
                picked = list.get(0);
                variableValue = art.get(index).get(picked);
                articleImg.setImageResource(getResources().getIdentifier(variableValue, "drawable", getPackageName()));
                break;
            case "history":
                list = new ArrayList<String>(history.get(index).keySet());
                picked = list.get(0);
                variableValue = history.get(index).get(picked);
                articleImg.setImageResource(getResources().getIdentifier(variableValue, "drawable", getPackageName()));
                break;
            case "maths":
                list = new ArrayList<String>(maths.get(index).keySet());
                picked = list.get(0);
                variableValue = maths.get(index).get(picked);
                articleImg.setImageResource(getResources().getIdentifier(variableValue, "drawable", getPackageName()));
                break;
            case "physics":
                list = new ArrayList<String>(physics.get(index).keySet());
                picked = list.get(0);
                variableValue = physics.get(index).get(picked);
                articleImg.setImageResource(getResources().getIdentifier(variableValue, "drawable", getPackageName()));
                break;
            case "code":
                list = new ArrayList<String>(coding.get(index).keySet());
                picked = list.get(0);
                variableValue = coding.get(index).get(picked);
                articleImg.setImageResource(getResources().getIdentifier(variableValue, "drawable", getPackageName()));
                break;
            default:
                System.out.println("Switch getArticle Probs here.");
        }

        return picked;
    }

}

