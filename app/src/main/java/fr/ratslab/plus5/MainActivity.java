package fr.ratslab.plus5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //On cache la barre de status pour obtenir un affichage plein écran
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }
    public void launchArt(View view) {
                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                String strName = "art";
                intent.putExtra("type", strName);

                MainActivity.this.startActivity(intent);
    }

    public void launchHistory(View view) {
                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                String strName = "history";
                intent.putExtra("type", strName);

                MainActivity.this.startActivity(intent);
    }

    public void launchMaths(View view) {
                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                String strName = "maths";
                intent.putExtra("type", strName);

                MainActivity.this.startActivity(intent);
    }

    public void launchPhysics(View view) {
                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                String strName = "physics";
                intent.putExtra("type", strName);

                MainActivity.this.startActivity(intent);
    }

    public void launchCode(View view) {
                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                String strName = "code";
                intent.putExtra("type", strName);

                MainActivity.this.startActivity(intent);
    }


}
