package fr.ratslab.plus5;

/**
 * Created by sebastientosello on 25/04/16.
 */
public class ParsedDataSet {
    private String parent = null;
    private String titre = null;
    private String article = null;

    public String getParent(){
        return parent;
    }

    public void setParent(String parent){
        this.parent = parent;
    }
    public String getTitre() {
        return titre;
    }

    public String getArticle(){
        return article;
    }

    public void setArticle(String parentTag) {
        this.article = parentTag;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

}