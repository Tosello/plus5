/!\ Connexion internet requise pour voir le contenu des articles. 

Nous avons donc une application qui présente l’opportunité d’apprendre 5 nouvelles choses (ou plus) sur 5 sujets différents à l’utilisateur. Le but étant de faire une application assez simple, on a ici 5 boutons pointant vers la même activité. L’activité cible s’adaptera en fonction du sujet choisi et présentera des articles wikipédia aléatoires « parsés » depuis leur api. 

D’un point de vue un peu plus technique l’activité principale qui fait office de menu, en cliquant sur un bouton et en ouvrant l’activité article, utilise un intent pour signifier quel sujet est choisi, ensuite les composants de l’ui seront remplacés par les valeurs récupérés aléatoirement parmi les propositions préalablement enregistrés dans le code.

Réalisée dans le cadre du Google Study Jam Android for Beginners 